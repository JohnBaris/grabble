package com.example.ioannis.grabble;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;


public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    static private String TAG = "MainActivity";
    private MediaPlayer player;
    private AssetFileDescriptor afd;


    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Log.i(TAG, "Grabble is running.");

        // Create a new GoogleApiClient
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        try {
            // Read the music file from the asset folder
            afd = getAssets().openFd("Main.mp3");
            // Creation of new media player;
            player = new MediaPlayer();
            // Set the player music source.
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),afd.getLength());
            // Set the looping and play the music.
            player.setLooping(true);
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Button located in the initial screen. When clicked, the user is directed to the game screen. A loading screen is displayed
        // until the game is fully loaded and then the game screen (map) comes up.
        Button startGameB = (Button) (findViewById(R.id.startButton));
        startGameB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Button was clicked");
                sendMessage(v);
            }


        });
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        player.start();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        player.pause();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Location services connected.");
    }

    public void sendMessage(View view) {
        // Get network/internet status. If there is no internet connection, then a message alert is displayed to the user
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo==null){
            showSettingsAlert();
        }
        else {
            // Proceed with the Loading/Splash Screen until the game begins. This happens when the button in the initial screen is clicked.
            Intent loadingIntent = new Intent(this, LoadingActivity.class);
            startActivity(loadingIntent);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // Make a request of the user to connect to the network/internet. First, the user is prompted to connect to a mobile network, and if
    // they decline they are then prompted to connect to a Wi-Fi connection. The user can't start the game without internet connection.
    public void showSettingsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder.setMessage("You need a network connection to use this application. Please turn on mobile network in Settings.")
                .setTitle("Unable to connect")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                startActivity(i);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                builder2.setMessage("You need a network connection to use this application. Please turn on Wi-Fi in Settings.")
                                        .setTitle("Unable to connect")
                                        .setCancelable(false)
                                        .setPositiveButton("Settings",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                                        startActivity(i);
                                                    }
                                                }
                                        )
                                        .setNegativeButton("Cancel",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                    }
                                                }
                                        );
                                AlertDialog alert2 = builder2.create();
                                alert2.show();
                            }
                        }
                );

        AlertDialog alert = builder.create();
        alert.show();
    }
}



