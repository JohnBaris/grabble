package com.example.ioannis.grabble;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ToggleButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.kml.KmlLayer;
import com.google.maps.android.kml.KmlPlacemark;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener, LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private static String TAG = "MapsActivity";
    private Marker myMarker;
    private ArrayList<Marker> markers = new ArrayList<>(); // List of the markers
    private ArrayList<Marker> markersVib = new ArrayList<>(); //List of the markers that have not been involved in a vibration
    private String letter;
    private MediaPlayer player;
    private AssetFileDescriptor afd;
    private LocationRequest mLocationRequest;
    private boolean gotLetter; // Boolean value which is true if the user got a letter
    private LatLng latLng;
    private String title;
    private int date;
    private boolean autoMode = false; // Boolean value which if true, it enables automatic zoom to user's current location when location is changed
    private static final CharSequence[] MAP_TYPE_ITEMS =
            {"Road Map", "Hybrid", "Satellite", "Terrain"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        // Create new GoogleApiClient
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        try {
            // Read the music file from the asset folder
            afd = getAssets().openFd("Map.mp3");
            // Creation of new media player;
            player = new MediaPlayer();
            // Set the player music source.
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            // Set the looping and play the music.
            player.setLooping(true);
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this); // Enable marker click
        InputStream input = null;
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK); // Current day
        // StrictMode.ThreadPolicy used for restricting Network Exception errors
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Read the Date file which contains the most recent date when the app was opened
        try {
            InputStream inputStream = getApplicationContext().openFileInput("Date");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            date = bufferedReader.read();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Get the kml file and add markers in the placemarks' positions
        Log.i("Saved Date",Integer.toString(date));
        Log.i("Current Date",Integer.toString(day));

        // If the last saved date is different from the current date, then load the suitable kml file for the current date
        // and save the current date to the Date file. Otherwise, do nothing.
        if(date!=day) {
            try {

                input = checkDate(day); // Get the kml file for the current date

                // Save the current date to the Date file
                File file = new File(getApplicationContext().getFilesDir(), "Date");
                if(!file.exists())
                {
                    try {
                        // Create the Date file if it doesn't exist
                        file.createNewFile();
                        OutputStream outputStream = openFileOutput("Date", getApplicationContext().MODE_PRIVATE);
                        outputStream.write(date);
                        outputStream.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    OutputStream outputStream = openFileOutput("Date", getApplicationContext().MODE_PRIVATE);
                    outputStream.write(date);
                    outputStream.close();
                }

                if(input != null) {

                    KmlLayer kmlLayer = new KmlLayer(mMap, input, getApplicationContext());

                    // Parse the description, name and coordinates of each placemark and add a respective marker with the same details
                    // to the map
                    for (KmlPlacemark placemark : kmlLayer.getPlacemarks()) {

                        String snippet = placemark.getProperty("description");
                        String title = placemark.getProperty("name");

                        //Parse the coordinates
                        String latnlng = placemark.getGeometry().getGeometryObject().toString();
                        ArrayList myList = new ArrayList(Arrays.asList(latnlng.split("\\(")));
                        String lng1 = myList.get(1).toString();
                        ArrayList myList1 = new ArrayList(Arrays.asList(lng1.split(",")));
                        String latitude = myList1.get(0).toString();
                        String lng2 = myList1.get(1).toString();
                        ArrayList myList2 = new ArrayList(Arrays.asList(lng2.split("\\)")));
                        String longitude = myList2.get(0).toString();
                        //Log.i(latitude, longitude);
                        myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude))).title(title).snippet(snippet));
                        markers.add(myMarker);
                        markersVib.add(myMarker);
                    }
                    //Log.i("Initial markers",Integer.toString(markers.size()));
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
        }

        // If the list of markers is empty, it means that the last saved date is the same as the current date, and then the saved kml file's
        // details are read and loaded from the latlngpoints.txt file.
        if (markers.isEmpty()) {
            try {
                FileInputStream input1 = openFileInput("latlngpoints.txt");
                DataInputStream din = new DataInputStream(input1);
                int sz = din.readInt(); // Read line count
                for (int i = 0; i < sz; i++) {
                    String str = din.readUTF();
                    //Log.v("read", str);
                    String[] stringArray = str.split(",");
                    double latitude = Double.parseDouble(stringArray[0]);
                    double longitude = Double.parseDouble(stringArray[1]);
                    String title = stringArray[2];
                    if(stringArray.length>3){
                        String snippet = stringArray[3];
                        myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(title).snippet(snippet));
                    }
                    else{
                        myMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(title));
                    }
                    markers.add(myMarker);
                    markersVib.add(myMarker);
                }
                din.close();
            } catch (IOException exc) {
                exc.printStackTrace();
            }

        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        // Show my current location
        mMap.setMyLocationEnabled(true);


        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        // 'Search' button. When clicked, the user is directed to the searched location, if a location was typed in the box,
        // otherwise, nothing happens.
        Button btnSearch = (Button) (findViewById(R.id.Bsearch));
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSearch(v);
            }
        });
        // 'Bag' button. When clicked, the user is directed to the Bag (GameActivity).
        Button bagButton = (Button) (findViewById(R.id.bagButton));
        bagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bagIntent = new Intent(getApplicationContext(), GameActivity.class);
                startActivity(bagIntent);
            }
        });

        // 'Auto' button. When toggled to on, automatic zoom to user's location, each time their locations is changed, is enabled.
        // When toggled to off, automatic zoom is disabled.
        ToggleButton auto = (ToggleButton) findViewById(R.id.autoB);
        auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    autoMode = true;
                } else {
                    // The toggle is disabled
                    autoMode = false;
                }
            }
        });

        // 'Map types' button. When clicked, it offers the user 4 different options of map types to choose from and it applies the chosen
        // map type on the map.
        Button mapTypeB = (Button) findViewById(R.id.mapType);
        mapTypeB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showMapTypeSelectorDialog();
            }
        });
    }

    // Search for the typed location
    public void onSearch(View view) {
        EditText location_ad = (EditText) findViewById(R.id.Address);
        String location = location_ad.getText().toString();
        List<Address> addressList = null;
        if (location != null && !location.equals("")) {
            //Log.i(TAG, location);
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        player.start(); // Music starts
    }

    protected void onStop() {
        // Save the current, perhaps modified (in case any letter was obtained), kml file's details in a latlngpoints.txt file
        try {
            FileOutputStream output = openFileOutput("latlngpoints.txt",
                    Context.MODE_PRIVATE);
            DataOutputStream dout = new DataOutputStream(output);
            dout.writeInt(markers.size()); // Save line count
            Log.i("Before write markers",Integer.toString(markers.size()));
            for (Marker point : markers) {
                if(point.getSnippet()!=null) {
                    dout.writeUTF(point.getPosition().latitude + "," + point.getPosition().longitude + "," + point.getTitle() + "," + point.getSnippet());
                    //Log.i("write", point.getPosition().latitude + "," + point.getPosition().longitude + "," + point.getTitle() + "," + point.getSnippet());
                }
                else{
                    dout.writeUTF(point.getPosition().latitude + "," + point.getPosition().longitude + "," + point.getTitle());
                    //Log.i("write", point.getPosition().latitude + "," + point.getPosition().longitude + "," + point.getTitle());
                }
            }
            dout.flush(); // Flush stream ...
            dout.close(); // ... and close.
        } catch (IOException exc) {
            exc.printStackTrace();
        }
        mGoogleApiClient.disconnect();
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        player.pause(); // Pause music
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Get user's current location
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,this);
            // Get latitude of the current location
            double latitude = mLastLocation.getLatitude();
            // Get longitude of the current location
            double longitude = mLastLocation.getLongitude();
            // Create a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);

            // Source: Joel Lipman site
            // Show the current location in Google Map
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            // Zoom in the Google Map
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
            LatLng myCoordinates = new LatLng(latitude, longitude);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(myCoordinates, 20);
            mMap.animateCamera(yourLocation);
        }
        // If location is null (GPS is turned off) when the map is loaded, then request of the user to turn GPS on.
        else {
            showSettingsAlert();
        }


    }

    // Request of the user to open the GPS services
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button, the user is directed to the settings in order to enable the GPS
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // If the user is connected to the internet and the GPS is on, then whenever a marker is clicked, its details become visible (this happens even when internet and GPS are off)
    // and the user is asked if they want to get the letter contained in it (only works when connected to internet and GPS), if a letter exists.
    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        Log.i("An actual marker ", "was clicked");
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        final LocationManager manager = (LocationManager) getSystemService( this.LOCATION_SERVICE );
        if(activeNetworkInfo!=null && manager.isProviderEnabled( LocationManager.GPS_PROVIDER)) {
            if (mLastLocation != null) {
                // Convert marker to location
                String title = marker.getTitle();
                LatLng markerLatLng = marker.getPosition();
                Location markerLocation = new Location("");
                markerLocation.setLatitude(markerLatLng.latitude);
                markerLocation.setLongitude(markerLatLng.longitude);
                // Distance between current location and clicked marker
                if (mLastLocation.distanceTo(markerLocation) < 15) {
                    // User is asked if they want to get an existing letter oontained in the clicked marker
                    if (marker.getSnippet() != null) {
                        letter = marker.getSnippet();
                        showLetterAlert(marker); // Letter message alert
                    }
                }
            }
        }
        return true;
    }


    // Ask user if they want to get the letter contained within the clicked marker
    public void showLetterAlert(Marker marker) {

        myMarker = marker;
        title = myMarker.getTitle();
        latLng = myMarker.getPosition();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("Letter");

        // Setting Dialog Message
        alertDialog.setMessage("Do you want to get letter " + myMarker.getSnippet() + "?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing yes, the user is directed to GameActivity and a message that they got the letter is shown.
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                gotLetter = true;
                myMarker.remove();
                markers.remove(myMarker);
                Marker testMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)).title(title));
                markers.add(testMarker);
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra("Extra_Letter", letter);
                Log.i(TAG, letter);
                startActivity(intent);
            }
        });

        // On pressing no
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                gotLetter = false;
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    // Get the kml file for the current date
    public InputStream checkDate(int day) {
        InputStream input = null;
        try {
            switch (day) {
                case Calendar.SUNDAY:
                    input = new URL("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/sunday.kml").openStream();
                    date = day;
                    break;
                case Calendar.MONDAY:
                    input = new URL("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/monday.kml").openStream();
                    date = day;
                    break;
                case Calendar.TUESDAY:
                    input = new URL("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/tuesday.kml").openStream();
                    date = day;
                    break;
                case Calendar.WEDNESDAY:
                    input = new URL("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/wednesday.kml").openStream();
                    date = day;
                    break;
                case Calendar.THURSDAY:
                    input = new URL("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/thursday.kml").openStream();
                    date = day;
                    break;
                case Calendar.FRIDAY:
                    input = new URL("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/friday.kml").openStream();
                    date = day;
                    break;
                case Calendar.SATURDAY:
                    input = new URL("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/saturday.kml").openStream();
                    date = day;
                    break;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    // Method which is accessed every time the user's location changes.
    @Override
    public void onLocationChanged(Location location) {
        Log.i("Location","changed");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if(mLastLocation!=null) {
            double latitude = mLastLocation.getLatitude();
            // Get longitude of the current location
            double longitude = mLastLocation.getLongitude();
            // Create a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);

            // If the user's location is out of the coordinates where the game can be played, then a message alert pops up.
            if(!(55.942617<=latitude && latitude<=55.946233) || (!(longitude<=-3.184319 && -3.192473<=longitude))){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Out of bounds!");

                // Setting Dialog Message
                alertDialog.setMessage("You are out of bounds! You can't play the game here!");

                // Setting Icon to Dialog
                //alertDialog.setIcon(R.drawable.delete);

                // On pressing yes
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }
            else {
                // If auto mode is true then enable automatic zoom to user's location
                if (autoMode == true) {
                    // Joel Lipman site
                    // Show the current location in Google Map
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    // Zoom in the Google Map
                    //mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
                    LatLng myCoordinates = new LatLng(latitude, longitude);
                    CameraUpdate yourLocation = CameraUpdateFactory.newLatLng(myCoordinates);
                    mMap.animateCamera(yourLocation);
                }

                // For each marker that has not yet been involved in a vibration, if it lies within a specific distance from the user's
                // location and it contains a letter, then initiate a vibration and remove it from the markersVib list.
                for (Marker mk : markersVib) {
                    LatLng markerLatLng = mk.getPosition();
                    Location markerLocation = new Location("");
                    markerLocation.setLatitude(markerLatLng.latitude);
                    markerLocation.setLongitude(markerLatLng.longitude);
                    // Distance between current location and marker
                    if (mLastLocation.distanceTo(markerLocation) < 15 && mk.getSnippet() != null) {
                        markersVib.remove(mk);
                        Vibrator v = (Vibrator) this.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        // Vibrate for 500 milliseconds
                        v.vibrate(500);
                        break;
                    }
                }
            }
        }
    }

    // Source: http://stackoverflow.com/questions/7064857/making-an-android-map-menu-to-change-map-type
    private void showMapTypeSelectorDialog() {
        // Prepare the dialog by setting up a Builder.
        final String fDialogTitle = "Select Map Type";
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(fDialogTitle);

        // Find the current map type to pre-check the item representing the current state.
        int checkItem = mMap.getMapType() - 1;

        // Add an OnClickListener to the dialog, so that the selection will be handled.
        builder.setSingleChoiceItems(
                MAP_TYPE_ITEMS,
                checkItem,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int item) {
                        // Locally create a finalised object.

                        // Perform an action depending on which item was selected.
                        switch (item) {
                            case 1:
                                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                                break;
                            case 2:
                                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                                break;
                            case 3:
                                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                                break;
                            default:
                                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        }
                        dialog.dismiss();
                    }
                }
        );

        // Build the dialog and show it.
        AlertDialog fMapTypeDialog = builder.create();
        fMapTypeDialog.setCanceledOnTouchOutside(true);
        fMapTypeDialog.show();
    }
}

