package com.example.ioannis.grabble;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class GameActivity extends AppCompatActivity {

    static private String TAG = "GameActivity";
    private String letter = null;
    private MediaPlayer player;
    private AssetFileDescriptor afd;
    private FileOutputStream outputStream;

    private ArrayList<String> dictionary = new ArrayList<>(); //List of words contained in the Grabble dictionary
    private ArrayList<String> letters = new ArrayList<>(); // List of letters
    private ArrayList<String> lettersTemp = new ArrayList<>(); // List of letters used for forming the words so that the 'letters' list does not get messed up
    private ArrayList<String> words = new ArrayList<>(); // List of words
    private ArrayList<String> awards = new ArrayList<>(); // List of awards
    private HashMap<String,Integer> letterVals = new HashMap<>(); // HashMap which contains the letters and their corresponding values-points
    private int totalPoints = 0;
    private int totalLetters = 0;
    private int totalWords = 0;
    private int currentPoints = 0; // Number of points achieved by current word formation
    private boolean emptyLetterList = true; // Boolean value which is true if the list of letters is empty

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        assignLVals(); // Assign the letters and their corresponding values-points to the HashMap
        // Read the files and load their content
        totalLetters = readScoreFile("Total Letters");
        totalWords= readScoreFile("Total Words");
        totalPoints = readScoreFile("Total Points");

        // Read the Letters file and save the letters contained in it to the list of letters.
        try {
            InputStream inputStream = getApplicationContext().openFileInput("Letters");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ( (receiveString = bufferedReader.readLine()) != null ) {
                stringBuilder.append(receiveString);
            }
            inputStream.close();
            if(stringBuilder.toString()!=null) {
                if(stringBuilder.toString().length()==1){
                    letters = new ArrayList(Arrays.asList(stringBuilder.toString()));
                    lettersTemp = new ArrayList(Arrays.asList(stringBuilder.toString()));
                }
                else {
                    letters = new ArrayList(Arrays.asList(stringBuilder.toString().split(",")));
                    lettersTemp = new ArrayList(Arrays.asList(stringBuilder.toString().split(",")));
                }
            }
            if (letters != null) {
                Log.i("Letters after read",letters.toString());
            }
            // Sometimes, before the obtainment of the first letter, an empty/null element was saved to the list of letters
            // (when an empty list of letters was saved to the Letters file) and this line exists to cope with that.
            if(letters.contains("")){
                letters = new ArrayList<>();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Read files and load the saved words and awards
        words = readWordsAwards("Words");
        awards = readWordsAwards("Awards");

        Bundle extras = getIntent().getExtras();
        // Check if the user collected a letter or clicked the 'bag' button
        if(extras == null) {
            letter= null;
        }
        // If the user collected a letter, then add it to the list of letter, increase the number of total letters and show a message.
        else {
            letter= extras.getString("Extra_Letter");
            letters.add(letter);
            lettersTemp.add(letter);
            totalLetters++;
            Log.i("Letters",letters.toString());
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You got letter " + letter).setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getLetterAward();
                        }
                    }
            );
            AlertDialog alert = builder.create();
            alert.show();
        }

        if(!letters.isEmpty()){
            emptyLetterList = false;
        }

        readDictionary();
        Log.i("Dictionary",dictionary.toString());
        //Log.i("Dictionary",dictionary.get(10000));


        try {
            // Read the music file from the asset folder
            afd = getAssets().openFd("Bag.mp3");
            // Creation of new media player;
            player = new MediaPlayer();
            // Set the player music source.
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),afd.getLength());
            // Set the looping and play the music.
            player.setLooping(true);
            player.prepare();
            player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 'Letters' button. When clicked, the list of letters currently owned is shown.
        Button letterB = (Button) (findViewById(R.id.letterButton));
        letterB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showLists(letters,"LETTERS");
            }
        });

        // 'Words' button. When clicked, the list of words that have been formed is shown.
        Button wordButton = (Button) (findViewById(R.id.wordButton));
        wordButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showLists(words,"WORDS");
            }
        });

        // 'Form Word' button. When clicked, the typed word is formed, if it is legal.
        Button wordB = (Button) (findViewById(R.id.wordB));
        wordB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                     formWord(v);
            }
        });

        // 'Scoreboard' button. When clicked, the scoreboard is shown. The scoreboard contains the total number of letters,
        // total number of words and total number of points currently achieved.
        Button scoreB = (Button) (findViewById(R.id.scoreButton));
        scoreB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage("Total Letters: " + totalLetters + ", Total Words: " + totalWords + ", Total Points:" + totalPoints, "SCORES");
            }
        });

        // 'Awards' button. When clicked, the list of awards-titles is shown.
        Button awardB = (Button) (findViewById(R.id.awardButton));
        awardB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLists(awards,"AWARDS");
            }
        });

        // 'Return' button. When clicked, the music stops and GameActivity finishes, directing the user to the map (MapActivity).
        Button returnButton = (Button) (findViewById(R.id.returnButton));
        returnButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                player.stop(); // Music stops
                finish(); // Activity finishes/stops
            }
        });
    }

    protected void onStart() {
        super.onStart();
        player.start(); // Music starts
    }

    protected void onStop() {
        // If the list of letter is not empty, then save it to the Letters file.
        if(!letters.isEmpty() || emptyLetterList == false) {
            Log.i("Letters before write", letters.toString());
            writeFile(letters,"Letters");
        }

        // If the list of words is not empty, then save it to the Words file.
        if(!words.isEmpty()) {
            writeFile(words,"Words");

        }

        // If the list of awards is not empty, then save it to the Awards file.
        if(!awards.isEmpty()){
            writeFile(awards,"Awards");
        }

        // Save the total number of letters, words and points to the corresponding files.
        writeScoreFile(totalLetters,"Total Letters");
        writeScoreFile(totalWords,"Total Words");
        writeScoreFile(totalPoints,"Total Points");


        super.onStop();
        player.pause(); // Pause music
    }

    public int checkPoints(String word){
        assignLVals();
        return letterVals.get(Character.toString(word.charAt(0))) + letterVals.get(Character.toString(word.charAt(1))) + letterVals.get(Character.toString(word.charAt(2))) + letterVals.get(Character.toString(word.charAt(3))) + letterVals.get(Character.toString(word.charAt(4))) + letterVals.get(Character.toString(word.charAt(5))) + letterVals.get(Character.toString(word.charAt(6)));
    }

    public boolean checkDictionary(String word){
        readDictionary();
        if(dictionary.contains(word)){
            return true;
        }
        return false;
    }

    // Read the dictionary
    public void readDictionary(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("grabble.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                dictionary.add(mLine.toUpperCase());
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
    }

    // Read the Words file and the Awards file and load the lists
    public ArrayList<String> readWordsAwards(String filename){
        ArrayList<String> ar = new ArrayList<>();
        try {
            InputStream inputStream = getApplicationContext().openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ( (receiveString = bufferedReader.readLine()) != null ) {
                stringBuilder.append(receiveString);
            }
            inputStream.close();
            if(stringBuilder.toString()!=null) {
                if(stringBuilder.toString().length()==7){
                    ar = new ArrayList(Arrays.asList(stringBuilder.toString()));
                }
                else {
                    ar= new ArrayList(Arrays.asList(stringBuilder.toString().split(",")));
                }
            }
            if (!ar.isEmpty()) {
                Log.i("Words after read",words.toString());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ar;
    }

    // Read the files that contain the Scoreboard values.
    public int readScoreFile(String filename){
        int score = 0;
        try {
            InputStream inputStream = getApplicationContext().openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            score = bufferedReader.read();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(filename,Integer.toString(score));
        return score;
    }

    // Save list 'ar' to file 'filename'.
    public void writeFile(ArrayList<String> ar, String filename){
        File file = new File(getApplicationContext().getFilesDir(), filename);
        if (!file.exists()) {
            try {
                file.createNewFile();
                outputStream = openFileOutput(filename, getApplicationContext().MODE_PRIVATE);
                StringBuilder stringBuilder = new StringBuilder();
                for (String s : ar) {
                    stringBuilder.append(s);
                }
                outputStream.write(stringBuilder.toString().getBytes());
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                OutputStream outputStream = openFileOutput(filename, getApplicationContext().MODE_PRIVATE);
                StringBuilder stringBuilder = new StringBuilder();
                int count = 0;
                for (String s : ar) {
                    stringBuilder.append(s);
                    count++;
                    if (count <= ar.size()) {
                        stringBuilder.append(",");
                    }
                }
                outputStream.write(stringBuilder.toString().getBytes());
                outputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Save score (number of letters/words/points) to file 'filename'.
    public void writeScoreFile(int score,String filename){
        File file = new File(getApplicationContext().getFilesDir(), filename);
        if(!file.exists())
        {
            try {
                file.createNewFile();
                OutputStream outputStream = openFileOutput(filename, getApplicationContext().MODE_PRIVATE);
                outputStream.write(score);
                outputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                OutputStream outputStream = openFileOutput(filename, getApplicationContext().MODE_PRIVATE);
                outputStream.write(score);
                outputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Show the appropriate list/message
    public void showMessage(String s, String name) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(GameActivity.this);

        final String testName = name;
        // Setting Dialog Title
        alertDialog.setTitle(name);

        // Setting Dialog Message
        alertDialog.setMessage(s);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing yes
        alertDialog.setPositiveButton("Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(testName=="The word was formed successfully!"){
                    getWordAward();
                }
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    // Show the appropriate list/message
    public void showLists(ArrayList<String> array, String name) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(GameActivity.this);

        final String testName = name;
        // Setting Dialog Title
        alertDialog.setTitle(name);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(GameActivity.this, android.R.layout.select_dialog_item);
        for(String s:array){
            arrayAdapter.add(s);
        }

        // Setting Dialog Message
        alertDialog.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing yes
        alertDialog.setPositiveButton("Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(testName=="The word was formed successfully!"){
                    getWordAward();
                }
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public boolean checkWord1(String word){
        if (word.length()==7){
            return true;
        }
        return false;
    }

    public boolean checkWord2(String word){
        if (word.length()==7){
            return true;
        }
        return false;
    }

    // Method for forming the words.
    public void formWord(View v){
        EditText wordText = (EditText) findViewById(R.id.editWord);
        String word = wordText.getText().toString().toUpperCase();
        if(lettersTemp!=null){
            Log.i("lettersTemp",lettersTemp.toString());
        }
        // If the word consists of 7 letters and the word is included in the Grabble dictionary, then check if the letters exist in the
        // list of letters, that is the user has obtained them. Each letter that is contained in the word and has been obtained, is removed
        // from the list, but it's added back if the formation of the word is unsuccessful because of the lack of some letters.
        if(word.length()==7 && dictionary.contains(word)){
            if(lettersTemp.contains(Character.toString(word.charAt(0)))){
                lettersTemp.remove(Character.toString(word.charAt(0)));
            }
            else{
                showMessage("Try to form another word","Invalid word");
                return;
            }
            if(lettersTemp.contains(Character.toString(word.charAt(1)))){
                lettersTemp.remove(Character.toString(word.charAt(1)));
            }
            else{
                lettersTemp.add(Character.toString(word.charAt(0)));
                showMessage("Try to form another word","Invalid word");
                return;
            }
            if(lettersTemp.contains(Character.toString(word.charAt(2)))){
                lettersTemp.remove(Character.toString(word.charAt(2)));
            }
            else{
                lettersTemp.add(Character.toString(word.charAt(0)));
                lettersTemp.add(Character.toString(word.charAt(1)));
                showMessage("Try to form another word","Invalid word");
                return;
            }
            if(lettersTemp.contains(Character.toString(word.charAt(3)))){
                lettersTemp.remove(Character.toString(word.charAt(3)));
            }
            else{
                lettersTemp.add(Character.toString(word.charAt(0)));
                lettersTemp.add(Character.toString(word.charAt(1)));
                lettersTemp.add(Character.toString(word.charAt(2)));
                showMessage("Try to form another word","Invalid word");
                return;
            }
            if(lettersTemp.contains(Character.toString(word.charAt(4)))){
                lettersTemp.remove(Character.toString(word.charAt(4)));
            }
            else{
                lettersTemp.add(Character.toString(word.charAt(0)));
                lettersTemp.add(Character.toString(word.charAt(1)));
                lettersTemp.add(Character.toString(word.charAt(2)));
                lettersTemp.add(Character.toString(word.charAt(3)));
                showMessage("Try to form another word","Invalid word");
                return;
            }
            if(lettersTemp.contains(Character.toString(word.charAt(5)))){
                lettersTemp.remove(Character.toString(word.charAt(5)));
            }
            else{
                lettersTemp.add(Character.toString(word.charAt(0)));
                lettersTemp.add(Character.toString(word.charAt(1)));
                lettersTemp.add(Character.toString(word.charAt(2)));
                lettersTemp.add(Character.toString(word.charAt(3)));
                lettersTemp.add(Character.toString(word.charAt(4)));
                showMessage("Try to form another word","Invalid word");
                return;
            }
            if(lettersTemp.contains(Character.toString(word.charAt(6)))){
                lettersTemp.remove(Character.toString(word.charAt(6)));
            }
            else{
                lettersTemp.add(Character.toString(word.charAt(0)));
                lettersTemp.add(Character.toString(word.charAt(1)));
                lettersTemp.add(Character.toString(word.charAt(2)));
                lettersTemp.add(Character.toString(word.charAt(3)));
                lettersTemp.add(Character.toString(word.charAt(4)));
                lettersTemp.add(Character.toString(word.charAt(5)));
                showMessage("Try to form another word","Invalid word");
                return;
            }

            // If the word formation is successful, then it is added in the list of words, the total number of words is increased by one,
            // the points achieved by the word formation is calculated, the number of total points gets increased by the number of current points
            // and the letters used in forming the word are removed/deleted from the list of letters.
            words.add(word);
            totalWords++;
            currentPoints = letterVals.get(Character.toString(word.charAt(0))) + letterVals.get(Character.toString(word.charAt(1))) + letterVals.get(Character.toString(word.charAt(2))) + letterVals.get(Character.toString(word.charAt(3))) + letterVals.get(Character.toString(word.charAt(4))) + letterVals.get(Character.toString(word.charAt(5))) + letterVals.get(Character.toString(word.charAt(6)));
            totalPoints = totalPoints + currentPoints;
            letters.remove(Character.toString(word.charAt(0)));
            letters.remove(Character.toString(word.charAt(1)));
            letters.remove(Character.toString(word.charAt(2)));
            letters.remove(Character.toString(word.charAt(3)));
            letters.remove(Character.toString(word.charAt(4)));
            letters.remove(Character.toString(word.charAt(5)));
            letters.remove(Character.toString(word.charAt(6)));
            showMessage(word + " was formed","The word was formed successfully!");
            // A TextView which shows the total number of points achieved.
            TextView pointsView = (TextView) (findViewById(R.id.points));
            pointsView.setText(currentPoints + " Points");
            pointsView.setTextSize(20);

        }
        else{
            showMessage("Try to form another word","Invalid word");
        }
    }

    // Assign values-points to letters.
    public void assignLVals(){
        letterVals.put("A",3);
        letterVals.put("B",20);
        letterVals.put("C",13);
        letterVals.put("D",10);
        letterVals.put("E",1);
        letterVals.put("F",15);
        letterVals.put("G",18);
        letterVals.put("H",9);
        letterVals.put("I",5);
        letterVals.put("J",25);
        letterVals.put("K",22);
        letterVals.put("L",11);
        letterVals.put("M",14);
        letterVals.put("N",6);
        letterVals.put("O",4);
        letterVals.put("P",19);
        letterVals.put("Q",24);
        letterVals.put("R",8);
        letterVals.put("S",7);
        letterVals.put("T",2);
        letterVals.put("U",12);
        letterVals.put("V",21);
        letterVals.put("W",17);
        letterVals.put("X",23);
        letterVals.put("Y",16);
        letterVals.put("Z",26);
    }

    // Get awards/titles for reaching certain numbers of collected letters.
    public void getLetterAward(){
        switch (totalLetters) {
            case 10:
                showMessage("You obtained the title of Letter Rookie!", "Title obtained");
                awards.add("Letter Rookie");
                break;
            case 25:
                showMessage("You obtained the title of Letter Farmer!", "Title obtained");
                awards.add("Letter Farmer");
                break;
            case 50:
                showMessage("You obtained the title of Letter Collector!", "Title obtained");
                awards.add("Letter Collector");
                break;
            case 100:
                showMessage("You obtained the title of Letter Knight!", "Title obtained");
                awards.add("Letter Knight");
                break;
            case 250:
                showMessage("You obtained the title of Letter Lord!", "Title obtained");
                awards.add("Letter Lord");
                break;
            case 500:
                showMessage("You obtained the title of Letter King!", "Title obtained");
                awards.add("Letter King");
                break;
            case 1000:
                showMessage("You obtained the title of Gof of Letters!", "Title obtained");
                awards.add("God of Letters");
                break;
        }
    }

    // Get awards/titles for reaching certain numbers of words collected.
    public void getWordAward(){
        switch (totalWords) {
            case 1:
                showMessage("You obtained the title of Word Beginner!", "Title obtained");
                awards.add("Word Beginner");
                break;
            case 5:
                showMessage("You obtained the title of Word Apprentice!", "Title obtained");
                awards.add("Word Apprentice");
                break;
            case 10:
                showMessage("You obtained the title of Word Alchemist!", "Title obtained");
                awards.add("Word Alchemist");
                break;
            case 20:
                showMessage("You obtained the title of Word Magician!", "Title obtained");
                awards.add("Word Magician");
                break;
            case 50:
                showMessage("You obtained the title of Word Wizard!", "Title obtained");
                awards.add("Word Wizard");
                break;
            case 100:
                showMessage("You obtained the title of Word Master!", "Title obtained");
                awards.add("Word Master");
                break;
        }
    }
}
