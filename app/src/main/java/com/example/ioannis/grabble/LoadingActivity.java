package com.example.ioannis.grabble;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;

public class LoadingActivity extends AppCompatActivity {

    private ProgressBar mProgress;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_loading);
        mProgress = (ProgressBar) findViewById(R.id.progressBar);

        // Create a delay handler to make the progress bar work. The set value (100) is a trivial delay.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startAnimation();
            }

        }, 100);
    }

    // Initialize the progress bar and make it load until it becomes full. Then, the game starts. The duration is set such that the loading screen
    // is displayed only for as long as the game takes to fully load.
    private void startAnimation() {
        final ProgressBar mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        int animProgress = 0;
        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(mProgressBar, "progress", 0, 100);
        progressAnimator.setDuration(2000);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                               @Override
                                               public void onAnimationUpdate(ValueAnimator animation) {
                                                   int anim = (Integer) animation.getAnimatedValue();
                                                   mProgressBar.setProgress(anim);
                                               }
                                           });
        progressAnimator.addListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd(Animator animation)
            {
                // done
                Intent mapIntent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(mapIntent);

            }
        });
        progressAnimator.start();

    }

}
