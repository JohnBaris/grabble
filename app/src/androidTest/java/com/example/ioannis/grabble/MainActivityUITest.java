package com.example.ioannis.grabble;

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Yannis on 17/01/2017.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityUITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule
            = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testMain() {

        SystemClock.sleep(3000);

        // "Start Game" button is clicked
        onView(withId(R.id.startButton))
                .perform(click());

        // "Check if progress bar (in loading screen) is displayed
        onView(withId(R.id.progressBar))
                .check(matches(isDisplayed()));

        SystemClock.sleep(5000);

        UiDevice device = UiDevice.getInstance(getInstrumentation());
        UiObject marker = device.findObject(new UiSelector().descriptionContains("Point"));

        try {
            marker.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }

        // A marker is clicked
        onView(withContentDescription("Google Map")).perform(click());


        SystemClock.sleep(3000);

        // Collect letter
        onView(withText("Yes")).perform(click());

        SystemClock.sleep(2000);

        onView(withText("Ok")).perform(click());

        SystemClock.sleep(2000);

        // Show list of letters
        onView(withId(R.id.letterButton))
                    .perform(click());

        SystemClock.sleep(3000);

        onView(withText("Back")).perform(click());

        SystemClock.sleep(2000);

        // Show list of words
        onView(withId(R.id.wordButton))
                .perform(click());

        SystemClock.sleep(2000);

        onView(withText("Back")).perform(click());

        SystemClock.sleep(2000);

        // A word is formed
        onView(withId(R.id.editWord))
                .perform(typeText("SFTORMW"), closeSoftKeyboard());

        SystemClock.sleep(2000);

        onView(withId(R.id.wordB))
                .perform(click());

        SystemClock.sleep(2000);

        onView(withText("Back")).perform(click());

        SystemClock.sleep(2000);

        // Show scoreboard
        onView(withId(R.id.scoreButton))
                .perform(click());

        SystemClock.sleep(2000);

        onView(withText("Back")).perform(click());

        // Show list of awards/titles
        onView(withId(R.id.awardButton))
                .perform(click());

        SystemClock.sleep(2000);


        onView(withText("Back")).perform(click());

        SystemClock.sleep(2000);

        // Close GameActivity (return to MapsActivity)
        onView(withId(R.id.returnButton))
                    .perform(click());

        SystemClock.sleep(3000);

        // "Bag" button is clicked
        onView(withId(R.id.bagButton))
                .perform(click());

        SystemClock.sleep(3000);

        // Close GameActivity (return to MapsActivity)
        onView(withId(R.id.returnButton))
                .perform(click());

        SystemClock.sleep(3000);

        // "Auto" button is clicked
        onView(withId(R.id.autoB))
                .perform(click());

        SystemClock.sleep(3000);

        // "Type" button is clicked
        onView(withId(R.id.mapType))
                .perform(click());

        SystemClock.sleep(3000);

        // "Terrain" type is chosen
        onView(withText("Terrain")).perform(click());

        SystemClock.sleep(5000);

    }

}
