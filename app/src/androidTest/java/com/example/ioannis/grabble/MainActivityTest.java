package com.example.ioannis.grabble;

import android.app.Instrumentation;
import android.support.test.filters.LargeTest;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;

/**
 * Created by Yannis on 16/01/2017.
 */

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
    }

    // Test the existence of "Start Game" button
    @LargeTest
    public void testMainButton(){
        Button bt = (Button)getActivity().findViewById(R.id.startButton);
        assertNotNull(bt);
    }

    // Test that when the "Start Game" button is clicked, then LoadingActivity starts
    @LargeTest
    public void testMainButtonNext(){
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(LoadingActivity.class.getName(), null, false);

        MainActivity myActivity = getActivity();
        final Button bt = (Button)getActivity().findViewById(R.id.startButton);
        myActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // click button and open next activity.
                bt.performClick();
            }
        });
        LoadingActivity nextActivity = (LoadingActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
        assertNotNull(nextActivity);
    }

    @Override
    protected void tearDown() throws Exception{
        super.tearDown();
    }
}
