package com.example.ioannis.grabble;

import android.support.test.filters.LargeTest;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Yannis on 16/01/2017.
 */

public class GameActivityTest extends ActivityInstrumentationTestCase2<GameActivity>{


    public GameActivityTest() {
        super(GameActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
    }

    @LargeTest
    public void testLetterButton(){
        Button letterB = (Button)getActivity().findViewById(R.id.letterButton);
        assertNotNull(letterB);
    }

    @LargeTest
    public void testWordButton(){
        Button wordB = (Button)getActivity().findViewById(R.id.wordButton);
        assertNotNull(wordB);
    }

    @LargeTest
    public void testFormButton(){
        Button formB = (Button)getActivity().findViewById(R.id.wordB);
        assertNotNull(formB);
    }

    @LargeTest
    public void testScoreButton(){
        Button scoreB = (Button)getActivity().findViewById(R.id.scoreButton);
        assertNotNull(scoreB);
    }

    @LargeTest
    public void testAwardButton(){
        Button awardB = (Button)getActivity().findViewById(R.id.awardButton);
        assertNotNull(awardB);
    }

    @LargeTest
    public void testReturnButton(){
        Button returnB = (Button)getActivity().findViewById(R.id.returnButton);
        assertNotNull(returnB);
    }


    @LargeTest
    public void testFormWord(){
        EditText formW = (EditText) getActivity().findViewById(R.id.editWord);
        assertNotNull(formW);
    }

    @Override
    protected void tearDown() throws Exception{
        super.tearDown();
    }
}
