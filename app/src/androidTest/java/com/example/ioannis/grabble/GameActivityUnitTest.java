package com.example.ioannis.grabble;

import android.content.Context;
import android.os.Looper;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;

import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Yannis on 16/01/2017.
 */

public class GameActivityUnitTest extends TestCase {

    @Override
    protected void setUp() throws Exception{
        super.setUp();
    }

    private Context ctx = InstrumentationRegistry.getTargetContext();

    // Tests the method that checks if a word consists 7 letters
    @LargeTest
    public void testcheckWord(){
        GameActivity ga = new GameActivity();
        boolean result = ga.checkWord1("MONSTER");
        assertEquals(true, result);
    }

    // Tests the method that checks if a word consists 7 letters
    @LargeTest
    public void testcheckWord2(){
        GameActivity ga = new GameActivity();
        boolean result = ga.checkWord1("FIEND");
        assertEquals(false, result);
    }

    // Tests if the word is contained in the dictionary. Same algorithm as the one used in GameActivity
    @LargeTest
    public void testcheckDictionary(){
        ArrayList dictionary = new ArrayList();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(ctx.getAssets().open("grabble.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                dictionary.add(mLine.toUpperCase());
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        boolean result = dictionary.contains("MONSTER");
        assertEquals(true, result);
    }

    @LargeTest
    public void testcheckDictionary2(){
        ArrayList dictionary = new ArrayList();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(ctx.getAssets().open("grabble.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                dictionary.add(mLine.toUpperCase());
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        boolean result = dictionary.contains("MSTONRE");
        assertEquals(false, result);
    }

    // Test the method that calculates the points
    @LargeTest
    public void testcheckPoints(){
        Looper.prepare();
        GameActivity ga = new GameActivity();
        int points = ga.checkPoints("MONSTER");
        assertEquals(points, 42);
    }


    @Override
    protected void tearDown() throws Exception{
        super.tearDown();
    }
}
