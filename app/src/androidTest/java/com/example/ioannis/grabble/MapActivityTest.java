package com.example.ioannis.grabble;

import android.app.Instrumentation;
import android.support.test.filters.LargeTest;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

/**
 * Created by Yannis on 17/01/2017.
 */

//  MapActivityTest interferes with MapsActivity . It may have negative side effects on the visibility of markers.
public class MapActivityTest extends ActivityInstrumentationTestCase2<MapsActivity>{
    public MapActivityTest() {
        super(MapsActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
    }

    @LargeTest
    public void testSearchButton(){
        Button searchB = (Button)getActivity().findViewById(R.id.Bsearch);
        assertNotNull(searchB);
    }

    @LargeTest
    public void testBagButton(){
        Button bagB = (Button)getActivity().findViewById(R.id.bagButton);
        assertNotNull(bagB);
    }

    //Test that when the "Bag" button is clicked, then GameActivity starts
    @LargeTest
    public void testBagButtonNext(){
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);

        MapsActivity myActivity = getActivity();
        final Button bt = (Button)getActivity().findViewById(R.id.bagButton);
        myActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // click button and open next activity.
                bt.performClick();
            }
        });
        GameActivity nextActivity = (GameActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 2000);
        assertNotNull(nextActivity);
    }

    @LargeTest
    public void testAutoButton(){
        ToggleButton autoB = (ToggleButton)getActivity().findViewById(R.id.autoB);
        assertNotNull(autoB);
    }

    @LargeTest
    public void testMapTypeButton(){
        Button typeB = (Button)getActivity().findViewById(R.id.mapType);
        assertNotNull(typeB);
    }

    @LargeTest
    public void testSearchEdit(){
        EditText searchEdit = (EditText) getActivity().findViewById(R.id.Address);
        assertNotNull(searchEdit);
    }

    @Override
    protected void tearDown() throws Exception{
        super.tearDown();
    }

}
